# Changelog

## Unreleased

### Fixed

- #5 Compatible SPIP 4.*
- #6 Perfs + style formulaire de sélection d'article d'accueil

### Changed

- Nécessite SPIP 4.2 minimum
- Chaînes de langue dans le format SPIP  4.1+

## 2.1.3 - 2024-09-22

### Fixed

- #3 Invalider le cache au changement d'article d'accueil
