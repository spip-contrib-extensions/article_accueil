<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/article_accueil?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// A
	'article_accueil' => 'Titulný článok',
	'aucun_article_accueil' => 'Žiaden článok',

	// L
	'label_id_article_accueil' => 'Titulný článok', # MODIF

	// R
	'rubrique_article_en_accueil' => 'Titulný článok:',
];
