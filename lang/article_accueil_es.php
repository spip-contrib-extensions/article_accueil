<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/article_accueil?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// A
	'article_accueil' => 'Artículo destacado',
	'aucun_article_accueil' => 'Ningún artículo',

	// L
	'label_id_article_accueil' => 'Artículo destacado', # MODIF

	// R
	'rubrique_article_en_accueil' => 'Artículo destacado actual:',
];
