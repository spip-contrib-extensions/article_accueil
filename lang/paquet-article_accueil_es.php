<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-article_accueil?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// A
	'article_accueil_description' => 'Este plugin te permite atribuir un artículo de inicio a las secciones. De ahí, se puede usar el campo id_article_accueil en los esqueletos.',
	'article_accueil_nom' => 'Artículos de portada',
	'article_accueil_slogan' => 'Atribuir un artículo de inicio a las secciones',
];
